using System;
using UnityEngine;

namespace Core.Scripts.GameObjectPool
{
    [Serializable]
    public class PrefabData
    {
        public GameObject Prefab;
        public bool IsActive;
        public string Type;

        public int Index;
    }
}