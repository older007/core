/*using System;
using System.Collections.Generic;
using System.Linq;
using Core.Scripts.Interfaces;
using Core.Scripts.Utils;
using UniRx;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Core.Scripts.GameObjectPool
{
    public class ObjectPoolv2 : IObjectPool, IInitable
    {
        private readonly Dictionary<GameObject, List<Object>> defaultPoolList = 
            new Dictionary<GameObject, List<Object>>();
        
        private readonly Dictionary<GameObject, GameObject> prefabsDictionary = 
            new Dictionary<GameObject, GameObject>();
        
        private readonly Queue<Action> instantiateQueue = new Queue<Action>();
        private GameObject ObjectPoolGameObject => objectPoolGameObject;
        private GameObject objectPoolGameObject;

        public void PreWarmPool(GameObject prefab, int poolSize, PrefabObject @params)
        {
            defaultPoolList.Add(prefab, new List<Object>());

            for (var i = 0; i < poolSize; i++)
            {
                Instantiate(prefab, @params.Data.Type);
            }
        }

        private GameObject GetFromPool(GameObject prefab)
        {
            if (!defaultPoolList.ContainsKey(prefab))
            {
                return default;
            }

            var item = defaultPoolList[prefab].FirstOrDefault();

            defaultPoolList[prefab].Remove(item);

            if (item as GameObject == null)
            {
                return default;
            }

            return item as GameObject;
        }

        public T GetFromPool<T>(GameObject prefab) where T : Object
        {
            if (!defaultPoolList.ContainsKey(prefab))
            {
                return default;
            }

            var item = defaultPoolList[prefab].FirstOrDefault();

            if (item as T == null)
            {
                return default;
            }

            defaultPoolList[prefab].Remove(item);
            
            return item as T;
        }

        public void Instantiate(GameObject prefab, Action<Object> action = null)
        {
            var gm = Object.Instantiate(prefab, ObjectPoolGameObject.transform);
            
            gm.SetActive(false);

            if (defaultPoolList.ContainsKey(prefab))
            {
                defaultPoolList[prefab].Add(gm);
            }
            else
            {
                defaultPoolList.Add(prefab, new List<Object>());
                defaultPoolList[prefab].Add(gm);
            }
            
            action.InvokeSafe(gm);
        }

        public void Instantiate<T>(GameObject prefab, Action<T> action = null) where T : Object
        {
            var fromPool = GetFromPool<T>(prefab);
            
            if (fromPool != default)
            {
                action?.InvokeSafe(fromPool);
                
                return;
            }

            instantiateQueue.Enqueue(Create);

            void Create()
            {
                var gm = Object.Instantiate(prefab, ObjectPoolGameObject.transform);
                var type = gm.GetComponent<T>();

                prefabsDictionary.Add(gm, prefab);

                gm.SetActive(false);

                if (defaultPoolList.ContainsKey(prefab))
                {
                    defaultPoolList[prefab].Add(type);
                }
                else
                {
                    defaultPoolList.Add(prefab, new List<Object>());
                    defaultPoolList[prefab].Add(type);
                }
                
                action?.InvokeSafe(type);
            }
        }
        
        private void Instantiate(GameObject prefab, string T = "GameObject", Action<Object> action = null)
        {
            var fromPool = GetFromPool(prefab);
            
            if (fromPool != default)
            {
                action?.InvokeSafe(fromPool);
                
                return;
            }

            instantiateQueue.Enqueue(Create);
            
            void Create()
            {
                var gm = Object.Instantiate(prefab, ObjectPoolGameObject.transform);
                var type = gm.GetComponent(T);

                prefabsDictionary.Add(gm, prefab);
                
                gm.SetActive(false);

                if (defaultPoolList.ContainsKey(prefab))
                {
                    defaultPoolList[prefab].Add(type);
                }
                else
                {
                    defaultPoolList.Add(prefab, new List<Object>());
                    defaultPoolList[prefab].Add(type);
                }
            
                action?.InvokeSafe(type);
            }

        }

        public void Destroy(GameObject objectToDestroy)
        {
            
        }

        public void Destroy<T>(GameObject objectToDestroy) where T : Object
        {
            objectToDestroy.transform.SetParent(ObjectPoolGameObject.transform);
            objectToDestroy.SetActive(false);

            var prefab = prefabsDictionary[objectToDestroy];
            
            defaultPoolList[prefab].Add(objectToDestroy.GetComponent<T>());
        }

        public void Init()
        {
            objectPoolGameObject = new GameObject(Constants.ObjectPoolParent);
            objectPoolGameObject.SetActive(false);

            Object.DontDestroyOnLoad(objectPoolGameObject);
            Observable.EveryLateUpdate().Subscribe(s => OnUpdate());
        }

        private void OnUpdate()
        {
            instantiateQueue.Dequeue()?.InvokeSafe();
        }
    }
}*/