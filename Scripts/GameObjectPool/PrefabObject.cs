﻿using System;
using UnityEngine;

namespace Core.Scripts.GameObjectPool
{
    [CreateAssetMenu(fileName = "Prefab_", menuName = "Core/SimplePrefab")]
    public class PrefabObject : ScriptableObject
    {
        public ObjectPoolParams PrefabParams;
        public PrefabData Data;
    }
}
