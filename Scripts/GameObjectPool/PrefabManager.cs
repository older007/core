﻿using Core.Scripts.Interfaces;
using Core.Scripts.Utils;
using Core.Scripts.Utils.ScriptableObjects;
using Zenject;

namespace Core.Scripts.GameObjectPool
{
    public class PrefabManager : IPrefabManager
    {
        public Prefabs Prefabs { get; }
        private IObjectPool objectPool;

        public PrefabManager(Prefabs prefabs, IObjectPool objectPool)
        {
            Prefabs = prefabs;
            
            this.objectPool = objectPool;
        }

        public void PreWarmPool()
        {
            foreach (var item in Prefabs.PrefabObjects)
            {
                objectPool.PreWarmPool(item.Data.Prefab, item.PrefabParams.PreWarmCount, item);
            }
        }

        public void Init()
        {
            PreWarmPool();
        }
    }
}