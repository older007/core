using System;
using Core.Scripts.Interfaces;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Core.Scripts.GameObjectPool
{
    public interface IObjectPool
    {
        void PreWarmPool(GameObject prefab, int poolSize, PrefabObject @params);
        void Instantiate(GameObject prefab, Action<GameObject> action = null);
        void Instantiate<T>(GameObject prefab, Action<T> action = null) where T : Component;
        //void Instantiate<T>(GameObject prefab, Action<T> action = null) where T : Object;
        void Destroy(GameObject objectToDestroy);
    }
}