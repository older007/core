﻿using System;
using System.Collections.Generic;
using Core.Scripts.Interfaces;
using Core.Scripts.Utils;
using UniRx;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;

namespace Core.Scripts.GameObjectPool
{
    public class ObjectPool : IObjectPool, IInitable
    {
        private readonly float _maxUpdateTime = 0.1f;

        private readonly Dictionary<GameObject, Queue<GameObject>> objectPools =
            new Dictionary<GameObject, Queue<GameObject>>();

        private readonly Dictionary<GameObject, GameObject> objectsPrefabs = 
            new Dictionary<GameObject, GameObject>();

        private readonly Dictionary<GameObject, ObjectPoolParams> poolParams =
            new Dictionary<GameObject, ObjectPoolParams>();

        private readonly List<GameObject> takenObjects = new List<GameObject>();
        private Transform poolParent;
        private GameObject gameObject;
        private float timeRunning;
        private Transform Transform => gameObject.transform;
        private string prefabName;
        
        private DiContainer diContainer;
        
        private GameObject ObjectPoolGameObject
        {
            get
            {
                if (objectPoolGameObject == null)
                {
                    objectPoolGameObject = new GameObject(Constants.ObjectPoolParent);
                    
                    Object.DontDestroyOnLoad(objectPoolGameObject);
                }

                return objectPoolGameObject;
            }
        }
        
        private GameObject objectPoolGameObject;
        
        public ObjectPool(DiContainer diContainer)
        {
            this.diContainer = diContainer;
        }

        private void Update()
        {
            timeRunning = Time.realtimeSinceStartup;

            foreach (var param in poolParams)
            {
                if (param.Value.OnFrameActions.Count == 0)
                {
                    continue;
                }

                param.Value.OnFrameActions.Dequeue().Invoke(param.Key);
            }
        }

        private GameObject AddToPool(GameObject prefab, bool isNew = false)
        {
            var result = diContainer.InstantiatePrefab(prefab, Vector3.zero, Quaternion.identity, poolParent);
            prefabName = $"{prefab.name}_{poolParams[prefab].Count++}";
            result.name = prefabName;
            objectsPrefabs.Add(result, prefab);

            if (isNew)
            {
                takenObjects.Add(result);
                result.GetComponent<IPoolable>()?.OnDespawned();
            }

            result.SetActive(poolParams[prefab].Activate);
            
            return result;
        }

        private GameObject TakeFromPool(GameObject prefab)
        {
            poolParams[prefab].TakeFromPoolCount--;
            var go = objectPools[prefab].Dequeue();
            takenObjects.Add(go);
            go.SetActive(poolParams[prefab].Activate);
            go.transform.SetParent(null);
            go.SendMessage(Constants.OnTakenFromPool, SendMessageOptions.DontRequireReceiver);
            go.GetComponent<IPoolable>()?.OnSpawned();
            
            return go;
        }

        public void PreWarmPool(GameObject prefab, int poolSize, PrefabObject @params)
        {
            if (prefab == null || poolParams.ContainsKey(prefab) || objectPools.ContainsKey(prefab))
            {
                return;
            }

            objectPools.Add(prefab, new Queue<GameObject>());
            poolParams.Add(prefab, new ObjectPoolParams()
            {
                Activate = @params.Data.IsActive,
                AutoExtend = true
            });

            for (var i = 0; i < poolSize; i++)
            {
                objectPools[prefab].Enqueue(AddToPool(prefab));
            }
        }
        
        public void Instantiate(GameObject prefab, Action<GameObject> action = null)
        {
            if (prefab == null)
            {
                return;
            }

            if (!poolParams.ContainsKey(prefab) || !objectPools.ContainsKey(prefab))
            {
                action?.Invoke(diContainer.InstantiatePrefab(prefab));
                return;
            }

            if (objectPools[prefab].Count - poolParams[prefab].TakeFromPoolCount <= 0)
            {
                if (!poolParams[prefab].AutoExtend)
                {
                    return;
                }

                poolParams[prefab].OnFrameActions.Enqueue(go => action?.Invoke(AddToPool(go, true)));
            }
            else
            {
                if (Time.realtimeSinceStartup - timeRunning < _maxUpdateTime)
                {
                    poolParams[prefab].TakeFromPoolCount++;
                    action?.Invoke(TakeFromPool(prefab));
                }
                else
                {
                    poolParams[prefab].TakeFromPoolCount++;
                    poolParams[prefab].OnFrameActions.Enqueue(go => action?.Invoke(TakeFromPool(go)));
                }
            }
        }

        public void Instantiate<T>(GameObject prefab, Action<T> action = null) where T : Component
        {
            if (prefab == null)
            {
                return;
            }

            if (!poolParams.ContainsKey(prefab) || !objectPools.ContainsKey(prefab))
            {
                action?.Invoke(diContainer.InstantiatePrefab(prefab).GetComponent<T>());
                return;
            }

            if (objectPools[prefab].Count - poolParams[prefab].TakeFromPoolCount <= 0)
            {
                if (!poolParams[prefab].AutoExtend)
                {
                    return;
                }

                poolParams[prefab].OnFrameActions.Enqueue(go => action?.Invoke(AddToPool(go, true).GetComponent<T>()));
            }
            else
            {
                if (Time.realtimeSinceStartup - timeRunning < _maxUpdateTime)
                {
                    poolParams[prefab].TakeFromPoolCount++;
                    action?.Invoke(TakeFromPool(prefab).GetComponent<T>());
                }
                else
                {
                    poolParams[prefab].TakeFromPoolCount++;
                    poolParams[prefab].OnFrameActions.Enqueue(go => action?.Invoke(TakeFromPool(go).GetComponent<T>()));
                }
            }
        }

        public void Destroy(GameObject objectToDestroy)
        {
            if (!takenObjects.Contains(objectToDestroy))
            {
                if (!objectsPrefabs.ContainsKey(objectToDestroy))
                {
                    Object.Destroy(objectToDestroy);
                }

                return;
            }

            objectToDestroy.transform.SetParent(poolParent);
            objectToDestroy.SetActive(false);
            objectPools[objectsPrefabs[objectToDestroy]].Enqueue(objectToDestroy);
            takenObjects.Remove(objectToDestroy);
        }

        private void InitPool()
        {
            Transform.SetParent(null);
            
            var count = Transform.childCount;

            for (var i = count - 1; i >= 0; i--)
            {
                Destroy(Transform.GetChild(i).gameObject);
            }
            
            poolParent.SetParent(Transform);
            poolParent.gameObject.SetActive(false);
            poolParent.name = Constants.ObjectPoolParent;

            Observable.EveryEndOfFrame().Subscribe(x => { Update();});
        }

        public void Init()
        {
            gameObject = ObjectPoolGameObject;
            poolParent = ObjectPoolGameObject.transform;
            gameObject.SetActive(false);
            
            InitPool();
        }
    }
}