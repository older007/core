using Core.Scripts.GameObjectPool;
using Core.Scripts.Interfaces;
using Core.Scripts.Utils;
using Core.Scripts.Utils.ScriptableObjects;
using Core.Scripts.WebRequest;
using DG.Tweening;
using UnityEngine;
using Zenject;
using Constants = Core.Scripts.Utils.Constants;
using Logger = Core.Scripts.Utils.Logger.Logger;

namespace Core.Scripts
{
    public class GameInstaller : MonoInstaller
    {
        private void Quit()
        {
            this.Log("Quit");
            Application.Quit();
        }

        private void Setup()
        {
            DontDestroyOnLoad(this);

            Application.targetFrameRate = 60;
        }

        public override void InstallBindings()
        {
            var loggerSettings = Resources.Load<LoggerSettings>("LogSettings");
            
            Container.Bind<LoggerSettings>().FromInstance(loggerSettings).AsSingle();
            
            Container.Bind<Logger>().AsSingle().NonLazy();

            Container.BindInterfacesTo<ApiBehaviour>().AsSingle().NonLazy();

            Container.Bind<RoutineManager>().FromNewComponentOnRoot().AsSingle().NonLazy();

            StaticInit();
        }

        private void Awake()
        {
            Init();
            Setup();
        }

        protected virtual void Init()
        {
            EventChanelSubscribe();

            InitDependency();
        }

        protected virtual void InitDependency()
        {
            Container.ResolveAll<IInitable>().ForEach(f=>f.Init());
        }

        protected virtual void StaticInit()
        {
            CoreExtensions.Init(Container);
        }

        protected virtual void EventChanelSubscribe()
        {
            this.GetChannel().Subscribe(Constants.ApplicationQuit, Quit);
        }

        private void OnDestroy()
        {
            this.GetChannel().UnSubscribe(Constants.ApplicationQuit, Quit);
        }
    }
}