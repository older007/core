using System;
using UnityEngine;

namespace Core.Scripts.StateMachine
{
    public abstract class ViewController<TState> : MonoBehaviour where TState : Enum
    {
        public abstract TState ViewState { get; }

        public virtual void Activated()
        {
            
        }
        
        public virtual void DeActivated()
        {
            
        }
    }
}