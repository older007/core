using System;

namespace Core.Scripts.StateMachine
{
    public interface IStateMachine<TState> : IStateMachine
    {
        void Fire(TState state);
        TState CurrentState { get; }
        event Action<TState> StateChanged;
    }

    public interface IStateMachine
    {
        void PreviewsStateFire();
    }
}