﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Scripts.Utils;
using UnityEngine;
using Zenject;
using LogType = Core.Scripts.Utils.Logger.LogType;
using Object = UnityEngine.Object;

namespace Core.Scripts.StateMachine
{
    public class StateMachine<TState> : IStateMachine<TState> where TState : Enum
    {
        private TState startState;
        private TState previewsState;
        private Transform viewsParent;
        private readonly StateMachineQueue<TState> lastStates;
        private readonly List<ViewController<TState>> viewControllers;
        private readonly Dictionary<TState, List<Action>> subscribers = new Dictionary<TState, List<Action>>();
        protected Dictionary<TState, StateParams> ViewsParams = new Dictionary<TState, StateParams>();
        public TState CurrentState { get; private set; }
        public event Action<TState> StateChanged;

        public StateMachine(TState startState, Transform viewsParent)
        {
            CurrentState = this.startState = startState;
            this.viewsParent = viewsParent;
            lastStates = new StateMachineQueue<TState>();
            lastStates.EndPeek(startState);
            viewControllers = new List<ViewController<TState>>();
            
            ValidateViews();
            
            foreach (var controller in viewControllers)
            {
                controller.gameObject.SetActive(false);
            }

            InternalFire(CurrentState);
        }

        ~StateMachine()
        {
            viewControllers.Clear();
            subscribers.Clear();
            ViewsParams.Clear();
            previewsState = default;
            CurrentState = default;
        }

        private void ValidateViews()
        {
            foreach (var view in viewsParent.GetComponentsInChildren<ViewController<TState>>(true))
            {
                if (!view)
                {
                    continue;
                }
                
                viewControllers.Add(view);

                var state = view.ViewState;
                
                Subscribe(state, BaseViewSubscription);
            }   
            
            SetupParams();
        }

        protected virtual void BaseViewSubscription()
        {
            this.LogWarning($"{CurrentState}", LogType.StateMachine);
        }

        private void InternalFire(TState state)
        {
            if (subscribers.ContainsKey(state))
            {
                subscribers[state].ForEach(f=>f.InvokeSafe());
            }

            lastStates.EndPeek(state);

            StateChanged?.Invoke(state);
            
            ViewsActivation();
        }

        private void ViewsActivation()
        {
            foreach (var viewController in viewControllers)
            {
                if (Equals(viewController.ViewState, CurrentState))
                {
                    viewController.Activated();
                    viewController.gameObject.SetActive(true);
                }
                else
                {
                    if (ViewsParams.ContainsKey(viewController.ViewState))
                    {
                        var param = ViewsParams[viewController.ViewState];

                        if (param.Modal)
                        {
                            viewController.Activated();
                            viewController.gameObject.SetActive(true);
                        }
                    }
                    else
                    {
                        viewController.DeActivated();
                        viewController.gameObject.SetActive(false);
                    }
                }
            }
        }

        protected virtual void SetupParams()
        {
            
        }

        public void Fire(TState state)
        {
            if (CurrentState.Equals(state))
            {
                return;
            }

            previewsState = CurrentState;
            CurrentState = state;
            
            InternalFire(CurrentState);
        }

        public void PreviewsStateFire()
        {
            var state = lastStates.Enqueue();
            
            Fire(state);
        }

        public void Subscribe(TState state, Action callBack)
        {
            if (!subscribers.ContainsKey(state))
            {
                subscribers.Add(state, new List<Action>());
            }
            
            subscribers[state].Add(callBack);
        }

        public void UnSubscribe(TState state, Action callBack)
        {
            if (!subscribers.ContainsKey(state))
            {
                return;
            }

            if (!subscribers[state].Contains(callBack))
            {
                return;
            }

            subscribers[state].Remove(callBack);
        }
    }
}