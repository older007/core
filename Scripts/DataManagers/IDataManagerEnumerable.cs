using System;
using System.Collections.Generic;

namespace Core.Scripts.DataManagers
{
    public interface IDataManagerEnumerable<TData> : IEnumerable<TData> where TData : IGuided
    {
        void AddData(TData data);
        void RemoveData(TData data);
        void UpdateData(TData data);
        void SetData(IEnumerable<TData> data);
        TData GetData(Guid id);
        TData GetFirst();
    }
}