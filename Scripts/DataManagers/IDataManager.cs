namespace Core.Scripts.DataManagers
{
    public interface IDataManager<TData>
    {
        TData Data { get; }
        void SetData(TData data);
    }
}