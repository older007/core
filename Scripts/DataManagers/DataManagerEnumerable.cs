using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Scripts.Utils;

namespace Core.Scripts.DataManagers
{
    public abstract class DataManagerEnumerable<TData> : IDataManagerEnumerable<TData> where TData : IGuided
    {
        private Dictionary<Guid, TData> Data { get; set; } = new Dictionary<Guid, TData>();

        protected string DataSetted => Constants.DataSetted;
        protected abstract string DataRemoved { get; }
        protected abstract string DataAdded { get; }
        protected abstract string DataUpdated { get; }

        public void AddData(TData data)
        {
            if (Data.ContainsKey(data.Id))
            {
                return;
            }

            RaiseEvent(DataAdded, data);
            Data.Add(data.Id, data);
        }

        public void RemoveData(TData data)
        {
            if (!Data.ContainsKey(data.Id))
            {
                return;
            }
            
            RaiseEvent(DataRemoved, data);
            Data.Remove(data.Id);
        }

        public void UpdateData(TData data)
        {
            if (!Data.ContainsKey(data.Id))
            {
                return;
            }

            RaiseEvent(DataUpdated, Data[data.Id]);
            Data[data.Id] = data;
        }

        public void SetData(IEnumerable<TData> data)
        {
            foreach (var guided in data)
            {
                Data.Add(guided.Id, guided);
            }
        }

        public TData GetData(Guid id)
        {
            if (Data.ContainsKey(id))
            {
                return Data[id];
            }

            return default;
        }

        public TData GetFirst()
        {
            return Data.Values.ToList()[0];
        }

        private void RaiseEvent(string data, TData value)
        {
            this.GetChannel<TData>().RaiseEvent(data, value);
        }

        public IEnumerator<TData> GetEnumerator()
        {
            return Data.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}