using System;

namespace Core.Scripts.DataManagers
{
    public interface IGuided
    {
        Guid Id { get; set; }
    }
}