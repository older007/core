using Core.Scripts.Utils;

namespace Core.Scripts.DataManagers
{
    public class DataManager<TData> : IDataManager<TData>
    {
        public TData Data { get; private set; }

        public void SetData(TData data)
        {
            InternalSetData(data);
        }

        private void InternalSetData(TData data)
        {
            if (data == null)
            {
                return;
            }

            Data = data;
            
            DataSetted();
        }

        private void DataSetted()
        {
            this.GetChannel<TData>().RaiseEvent(Constants.DataUpdated, Data);
        }
    }
}