using System.Collections.Generic;
using UnityEngine;
using LogType = Core.Scripts.Utils.Logger.LogType;

namespace Core.Scripts.Utils.ScriptableObjects
{
    [CreateAssetMenu(menuName = "Core/Logger", fileName = "LogSettings")]
    public class LoggerSettings : ScriptableObject
    {
        [SerializeField] private List<LogType> acceptableLogLevels;
        [SerializeField] private bool clearLogsOnStart;
        public List<LogType> AcceptableLogLevels => acceptableLogLevels;
        public bool ClearLogsOnStart => clearLogsOnStart;
    }
}