using System.Collections.Generic;
using UnityEngine;

namespace Core.Scripts.Utils.ScriptableObjects
{
    [CreateAssetMenu(menuName = "Core/Scene", fileName = "Scene Settings")]
    public class SceneSettings<TParams> : ScriptableObject where TParams : SceneParams
    {
        [SerializeField] private List<TParams> sceneParams;

        public List<TParams> SceneParams => sceneParams;
    }
}