﻿using System.Collections.Generic;
using Core.Scripts.GameObjectPool;
using UnityEngine;

namespace Core.Scripts.Utils.ScriptableObjects
{
    [CreateAssetMenu(menuName = "Core/Prefabs", fileName = "Prefabs")]
    public class Prefabs : ScriptableObject
    {
        [SerializeField] private List<PrefabObject> prefabObjects;
        
        public List<PrefabObject> PrefabObjects => prefabObjects;
    }
}
