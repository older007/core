using System;

namespace Core.Scripts.Utils
{
    public class RandomUtils
    {
        private static Random generator; 
        
        public static void SplitAtRandom(int chanceOfSuccess, Action onSuccess = null, Action onFailure = null)
        {
            if (generator == null)
                generator = new Random(DateTime.Now.Millisecond);

            if (generator.Next(Constants.MaxRandomValue) < chanceOfSuccess)
            {
                onSuccess?.InvokeSafe();
            }
            else
            {
                onFailure?.InvokeSafe();
            }
        }
    }
}