namespace Core.Scripts.Utils
{
    public enum SceneType
    {
        Init,
        Game,
        Main,
        Loading,
        Other
    }
}