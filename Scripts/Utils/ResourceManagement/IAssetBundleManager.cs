using System;
using Object = UnityEngine.Object;

namespace Core.Scripts.Utils.ResourceManagement
{
    public interface IAssetBundleManager
    {
        void LoadAssetBundle<TData>(AssetBundleLoadSettings<TData> settings) where TData : Object;
        void Clear();
    }
}