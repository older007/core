using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;

namespace Core.Scripts.Utils.ResourceManagement
{
    public class AssetBundleManager : IAssetBundleManager
    {
        [Inject] 
        private RoutineManager routineManager;
        private readonly Dictionary<string, Object> assetBundles = new Dictionary<string, Object>();
        private readonly List<AssetBundle> tempAssetBundles = new List<AssetBundle>();

        public void LoadAssetBundle<TData>(AssetBundleLoadSettings<TData> settings) where TData : Object
        {
            if (assetBundles.ContainsKey(settings.Path))
            {
                settings.OnLoadedCallBack.Invoke(assetBundles[settings.Path] as TData);
                
                Debug.LogWarning($"{settings.Path} is loaded");
            }

            routineManager.RoutineStart(LoadBundle(settings));
        }

        public void Clear()
        {
            foreach (var bundle in tempAssetBundles)
            {
                bundle.Unload(true);
            }
        }

        private IEnumerator LoadBundle<TData>(AssetBundleLoadSettings<TData> settings) where TData : Object
        {
            var bundle = AssetBundle.LoadFromFile(settings.Path);

            yield return bundle;

            if (!bundle)
            {
                this.LogError($"Can't load asset bundle from {settings.Path}");
                yield break;
            }

            var assetBundle = bundle.LoadAsset(settings.AssetName, settings.AssetType);

            if (!assetBundle)
            {
                this.LogError($"Can't load {settings.AssetType} from asset bundle");
                yield break;
            }

            if (settings.IsPermanent)
            {
                if (!assetBundles.ContainsKey(settings.Path))
                {
                    assetBundles.Add(settings.Path, assetBundle);      
                    bundle.Unload(false);
                }
            }
            else
            {
                tempAssetBundles.Add(bundle);
            }
            
            settings.OnLoadedCallBack.InvokeSafe(assetBundle as TData);
        }
    }
}