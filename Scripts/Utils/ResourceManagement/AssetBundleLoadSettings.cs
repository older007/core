using System;
using Object = UnityEngine.Object;

namespace Core.Scripts.Utils.ResourceManagement
{
    public class AssetBundleLoadSettings<TType> where  TType : Object
    {
        public AssetBundleLoadSettings(string path, string assetName, bool isPermanent, Action<TType> onLoadedCallBack)
        {
            Path = path;
            AssetName = assetName;
            IsPermanent = isPermanent;
            this.OnLoadedCallBack = onLoadedCallBack;
        }

        public string Path { get; }
        public string AssetName { get; }
        public bool IsPermanent { get; }
        public Action<TType> OnLoadedCallBack { get; }
        public Type AssetType => typeof(TType);
    }
}