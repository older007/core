namespace Core.Scripts.Utils.Logger
{
    public enum LogType
    {
        Api,
        Routine,
        EventChannel,
        Other,
        Core,
        FileLogs,
        FileCrash,
        StateMachine,
        Inited
    }
}