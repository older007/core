using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Core.Scripts.Utils.ScriptableObjects;
using UnityEngine;

namespace Core.Scripts.Utils.Logger
{
    public class Logger
    {
        private string LogPath => Constants.NormalLogDirectory;
        private string CrashPath => Constants.CrashLogDirectory;
        
        private string LogFilePath =>
            $"{Application.persistentDataPath}/" +
            $"{Constants.LogFolder}/" +
            $"{Application.version}/" +
            $"Logs.txt";

        
        private string CrashFilePath => 
            $"{Application.persistentDataPath}/" +
            $"{Constants.CrashFolder}/" +
            $"{Application.version}/" +
            $"{DateTime.Today:dd-MM-yy}_{crashLogs.Count}.txt";

        private readonly Dictionary<string,string> crashLogs = new Dictionary<string, string>();
        private readonly Dictionary<LogLevel, Action<string, LogType>> log;
        private readonly LoggerSettings loggerSettings;
        private string errorMsg;
        private string fileMsg;
        
        public Logger(LoggerSettings settings)
        {
            loggerSettings = settings;

            if (!Directory.Exists(LogPath))
            {
                Directory.CreateDirectory(LogPath);
            }
            
            if (!Directory.Exists(CrashPath))
            {
                Directory.CreateDirectory(CrashPath);
            }
            
            if (!File.Exists(LogFilePath))
            {
                File.Create(LogFilePath).Dispose();
                File.SetAttributes(LogFilePath, FileAttributes.Normal);
            }
            
            log = new Dictionary<LogLevel, Action<string, LogType>>()
            {
                {
                    LogLevel.Warning, LogWarning
                },
                {
                    LogLevel.Error, LogError
                },
                {
                    LogLevel.Normal, LogNormal
                },
                {
                    LogLevel.File, LogFile
                }
            };
            
            Application.logMessageReceived += ApplicationOnLogMessageReceived;
        }

        private void ApplicationOnLogMessageReceived(string condition, string stacktrace, UnityEngine.LogType type)
        {
            if (type == UnityEngine.LogType.Error)
            {
                LogFileSingle(condition, stacktrace);
            }
        }

        public void Log(string msg, LogType type = LogType.Other , LogLevel level = LogLevel.Normal)
        {
            if (!loggerSettings.AcceptableLogLevels.Contains(type))
            {
                return;
            }

            log[level].InvokeSafe(msg, type);
        }

        private void LogNormal(string e, LogType t)
        {
            errorMsg = $"{t} \n {e}"; 
            Debug.Log(errorMsg);
        }
        
        private void LogWarning(string e, LogType t)
        {
            errorMsg = $"{t} \n {e}";
            Debug.LogWarning(errorMsg);
        }
        
        private void LogError(string e, LogType t)
        {
            errorMsg = $"{t} \n {e}";
            Debug.LogError(errorMsg);
        }
        
        private void LogFile(string e, LogType t)
        {
            if (!loggerSettings.AcceptableLogLevels.Contains(LogType.FileLogs))
            {
                return;
            }

            fileMsg = $"| {DateTime.Now:F} |\n" +
                      $"Log Level =={t.ToString()}== \n" +
                      $"Log msg : {e} \n" +
                      $"{Constants.EmptyLogLine} \n";
            
            File.AppendAllText(LogFilePath, fileMsg);
        }

        private void LogFileSingle(string condition, string stacktrace)
        {
            if (!loggerSettings.AcceptableLogLevels.Contains(LogType.FileCrash))
            {
                return;
            }
            
            if (!crashLogs.ContainsKey(condition))
            {
                if (!File.Exists(CrashFilePath))
                {
                    File.Create(CrashFilePath);
                    File.SetAttributes(CrashFilePath, FileAttributes.Normal);
                }
            
                fileMsg = $"{Constants.EmptyLogLine} \n" +
                          $"| {DateTime.Now:F} |\n" +
                          $"{condition}\n" +
                          $"{Constants.EmptyLogLine}\n" +
                          $"{stacktrace}" +
                          $"{Constants.EmptyLogLine}\n";                
                
                File.WriteAllText(CrashFilePath, fileMsg);

                crashLogs.Add(condition, stacktrace);
            }

            if (crashLogs.Count > Constants.CrashLogsCount)
            {
                crashLogs.Clear();
            }
        }
    }
}