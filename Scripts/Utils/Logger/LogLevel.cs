namespace Core.Scripts.Utils.Logger
{
    public enum LogLevel
    {
        Normal,
        Warning,
        Error,
        File
    }
}