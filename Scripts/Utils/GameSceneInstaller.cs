using Controllers;
using Interfaces;
using UnityEngine;

namespace Core.Scripts.Utils
{
    public class GameSceneInstaller : BaseGameSceneInstaller
    {
        [SerializeField] private Player player;
        public override void InstallBindings()
        {
            base.InstallBindings();

            Container.Bind<ITarget>().FromInstance(player).AsCached();
        }
    }
}