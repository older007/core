using UnityEngine;

namespace Core.Scripts.Utils
{
    [System.Serializable]
    public class SceneParams
    {
        [SerializeField] private string sceneName;
        [SerializeField] private SceneType sceneType;

        public string SceneName => sceneName;
        public SceneType SceneType => sceneType;
    }
}