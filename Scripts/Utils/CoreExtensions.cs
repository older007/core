using System;
using System.Threading.Tasks;
using Core.Scripts.EventChannel;
using Core.Scripts.Utils.Logger;
using Zenject;
using LogType = Core.Scripts.Utils.Logger.LogType;

namespace Core.Scripts.Utils
{
    public static class CoreExtensions
    {
        private static Logger.Logger Logger => diContainer.Resolve<Logger.Logger>();
        private static DiContainer diContainer;
        
        public static void Init(DiContainer container)
        {
            diContainer = container;
        }

        public static DiContainer GetServerContainer(this object anyObject)
        {
            return diContainer;
        }

        public static void Log(this object anyObject, string msg, LogType type = LogType.Other)
        {
            Logger.Log(msg, type);
        }

        /// <summary>
        /// Only for Crash Logs, automatically creating file with exception msg
        /// </summary>
        /// <param name="anyObject"></param>
        /// <param name="msg"></param>
        /// <param name="type"></param>
        public static void LogError(this object anyObject, string msg, LogType type = LogType.Other)
        {
            Logger.Log(msg, type, LogLevel.Error);
        }

        public static void LogWarning(this object anyObject, string msg, LogType type = LogType.Other)
        {
            Logger.Log(msg, type, LogLevel.Warning);
        }

        public static void LogFile(this object anyObject, string msg, LogType type = LogType.Other)
        {
            Logger.Log(msg, type, LogLevel.File);
        }

        public static IEventChannel GetChannel(this object anyObject)
        {
            if (diContainer.HasBinding<IEventChannel>())
            {
                return diContainer.Resolve<IEventChannel>();
            }

            var channel = new EventChannel.EventChannel();
            
            diContainer.Bind<IEventChannel>().To<EventChannel.EventChannel>().FromInstance(channel).AsSingle();

            return diContainer.Resolve<IEventChannel>();
        }

        public static IEventChannel<TArg> GetChannel<TArg>(this object anyObject)
        {
            if (diContainer.HasBinding<IEventChannel<TArg>>())
            {
                return diContainer.Resolve<IEventChannel<TArg>>();
            }

            var channel = new EventChannel<TArg>();
            
            diContainer.Bind<IEventChannel<TArg>>().To<EventChannel<TArg>>().FromInstance(channel).AsSingle();

            return diContainer.Resolve<IEventChannel<TArg>>();
        }
        
        public static IEventChannel<TArg1, TArg2> GetChannel<TArg1, TArg2>(this object anyObject)
        {
            if (diContainer.HasBinding<IEventChannel<TArg1, TArg2>>())
            {
                return diContainer.Resolve<IEventChannel<TArg1, TArg2>>();
            }

            var channel = new EventChannel<TArg1, TArg2>();
            
            diContainer.Bind<IEventChannel<TArg1, TArg2>>().To<EventChannel<TArg1, TArg2>>().FromInstance(channel).AsSingle();

            return diContainer.Resolve<IEventChannel<TArg1, TArg2>>();
        }
    }
}