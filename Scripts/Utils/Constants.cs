using System.IO;
using UnityEngine;

namespace Core.Scripts.Utils
{
    public partial class Constants
    {
        public const int MaxRandomValue = 100;
        public const int CrashLogsCount = 100;
        public const string Restart = "Restart";
        public const string ObjectPoolParent = "Pool";
        public const string LoadMainScene = "LoadMainScene";
        public const string LoadGameScene = "LoadGameScene";
        public const string OnTakenFromPool = "OnTakenFromPool";
        public const string ApplicationQuit = "ApplicationQuit";
        public const string RoutineManager = "RoutineManager";
        public const string LogFolder = "Logs";
        public const string CrashFolder = "CrashLogs";
        public const string CrashDirectory = "";
        public const string EmptyLogLine = "================================================";

        public static string NormalLogDirectory =
            Path.Combine(Application.persistentDataPath, LogFolder, Application.version);
        
        public static string CrashLogDirectory =
            Path.Combine(Application.persistentDataPath, CrashFolder, Application.version);

        //event channel
        
        public const string DataUpdated = "DataUpdated";
        public const string DataAdded = "DataAdded";
        public const string DataSetted = "DataSetted";
        public const string DataRemoved = "DataRemoved";
    }
}
