﻿using Controllers.Interfaces;
using Core.Scripts.GameObjectPool;
using Core.Scripts.Interfaces;
using Core.Scripts.Utils.ScriptableObjects;
using UnityEngine;
using Zenject;

namespace Core.Scripts.Utils
{
    public class BaseGameSceneInstaller : MonoInstaller
    {
        [SerializeField] private Prefabs scenePrefabs;
        public override void InstallBindings()
        {
            Container.Bind<Prefabs>().FromInstance(scenePrefabs).AsSingle().NonLazy();
            
            Container.BindInterfacesTo<ObjectPool>().AsSingle().NonLazy();
            Container.BindInterfacesTo<PrefabManager>().AsSingle().NonLazy();
            Container.BindInterfacesTo<KeyboardInputController>().AsSingle().NonLazy();
        }

        private void Awake()
        {
            Container.ResolveAll<IInitable>().ForEach(f=>f.Init());
        }
    }
}
