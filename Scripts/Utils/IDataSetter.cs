namespace Core.Scripts.Utils
{
    public interface IDataSetter<in TData>
    {
        void SetData(TData data);
    }
    
    public interface IDataSetter<in TData1, in TData2>
    {
        void SetData(TData1 data1, TData2 data2);
    }
}