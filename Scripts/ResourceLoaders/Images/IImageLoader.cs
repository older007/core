using UnityEngine.U2D;

namespace Core.Scripts.ResourceLoaders.Images
{
    public interface IImageLoader<in TParams>
    {
        bool LoadAtlas(TParams @params);
        void LoadDefaultAtlas(SpriteAtlas spriteAtlas);
    }
}