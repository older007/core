﻿using UnityEngine;
using UnityEngine.U2D;

namespace Core.Scripts.ResourceLoaders.Images
{
    public class ImageManager : IImageManager
    {
        protected SpriteAtlas currentAtlas;
        protected SpriteAtlas defaultAtlas;
    
        public Sprite GetSprite(string name)
        {
            if (currentAtlas == null || currentAtlas.GetSprite(name) == null)
            {
                return defaultAtlas.GetSprite(name);
            }

            var sprite = currentAtlas.GetSprite(name);

            return sprite;
        }

        public Sprite GetSpriteByIndex(int index)
        {
            var sprites = new Sprite[0];
            var spriteCount = currentAtlas.GetSprites(sprites);
            var sprite = sprites[index];

            return sprite;
        }
    }
}
