using UnityEngine;

namespace Core.Scripts.ResourceLoaders.Images
{
    public interface IImageManager
    {
        Sprite GetSprite(string name);
        Sprite GetSpriteByIndex(int index);
    }
}