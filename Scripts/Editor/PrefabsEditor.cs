using System.Linq;
using Core.Scripts.GameObjectPool;
using ModestTree;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Core.Scripts.Editor
{
    
    [CustomEditor(typeof(PrefabObject))]
    [CanEditMultipleObjects]
    public class PrefabsEditor : UnityEditor.Editor
    {
        PrefabObject target => serializedObject.targetObject as PrefabObject;
        private PrefabData data => target.Data;
        
        public override void OnInspectorGUI()
        {
            EditorGUILayout.BeginHorizontal();

            DrawDefault();
            
            EditorGUILayout.EndHorizontal();
        }

        private void DrawDefault()
        {
            EditorGUILayout.BeginVertical();

            data.Prefab = (GameObject)EditorGUILayout.ObjectField("Prefab :", data.Prefab,typeof(GameObject), false);
            data.IsActive = EditorGUILayout.Toggle("Is Active", data.IsActive);
            target.PrefabParams.PreWarmCount = EditorGUILayout.IntField("Count :", target.PrefabParams.PreWarmCount);
            
            DrawComponentsChose();
            
            EditorGUILayout.EndVertical();
        }

        private void DrawComponentsChose()
        {
            if (!data.Prefab)
            {
                return;
            }

            var components = data.Prefab.GetComponents<Component>();
            var names = components.Select(s => s.GetType().Name).ToArray();

            data.Index = EditorGUILayout.Popup(data.Index, names);
            data.Type = components[data.Index].GetType().Name;
        }
    }
}