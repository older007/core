using Core.Scripts.Utils.ScriptableObjects;
using Zenject;

namespace Core.Scripts.Interfaces
{
    public interface IPrefabManager : IInitable
    {
        Prefabs Prefabs { get; }
        void PreWarmPool();
    }
}