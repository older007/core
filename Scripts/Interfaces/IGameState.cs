namespace Core.Scripts.Interfaces
{
    public interface IGameState
    {
        bool GameState { get; }
    }
}