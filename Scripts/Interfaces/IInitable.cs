namespace Core.Scripts.Interfaces
{
    public interface IInitable
    {
        void Init();
    }
}