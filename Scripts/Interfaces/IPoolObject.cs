namespace Core.Scripts.Interfaces
{
    public interface IPoolObject : IInitable
    {
        void Destroying();
    }
}