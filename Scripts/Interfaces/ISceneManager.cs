using System;

namespace Core.Scripts.Interfaces
{
    public interface ISceneManager : IInitable
    {
        void LoadMainScene();
        void LoadLoadingScene();
        void LoadScene(string name, Action callBack = null, Action<float> progressCallback = null);
    }
}