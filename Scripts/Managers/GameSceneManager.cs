﻿using System;
using System.Collections;
using Core.Scripts.Interfaces;
using Core.Scripts.ResourceLoaders.Images;
using Core.Scripts.Utils;
using Core.Scripts.Utils.ScriptableObjects;
using UnityEngine.SceneManagement;
using Zenject;
using SceneManager = UnityEngine.SceneManagement.SceneManager;

namespace Core.Scripts.Managers
{
    public class GameSceneManager<TParams> : ISceneManager where TParams : SceneParams
    {
        protected readonly SceneSettings<TParams> sceneSettings;
        protected string currentSceneName;
        
        [Inject] protected RoutineManager routineManager;
        [Inject] protected ZenjectSceneLoader sceneLoader;
        [Inject] protected IImageLoader<TParams> imageManager;

        private SceneParams mainScene => sceneSettings.SceneParams.Find(f => f.SceneType == SceneType.Main);
        private SceneParams loadingScene => sceneSettings.SceneParams.Find(f => f.SceneType == SceneType.Loading);
        public GameSceneManager(SceneSettings<TParams> settings)
        {
            sceneSettings = settings;

            currentSceneName = settings.SceneParams.Find(f=>f.SceneType == SceneType.Init).SceneName;

            SceneManager.sceneLoaded += (arg0, mode) => { SceneLoaded(arg0.name); };
        }

        public void LoadMainScene()
        {
            LoadScene(mainScene.SceneName, () => this.Log("Main scene loaded"));
        }

        public void LoadLoadingScene()
        {
            sceneLoader.LoadScene(loadingScene.SceneName, LoadSceneMode.Single);
        }

        public void LoadScene(string name, Action callBack = null, Action<float> progressCallback = null)
        {
            routineManager.RoutineStart(AsyncLoadScene(name, callBack, progressCallback));
        }

        private IEnumerator AsyncLoadScene(string name, Action callBack, Action<float> progressCallback = null)
        {
            LoadLoadingScene();
            
            currentSceneName = name;

            var loading = sceneLoader.LoadSceneAsync(name, LoadSceneMode.Single);
            
            SceneLoading(name);
            
            while (!loading.isDone)
            {
                progressCallback.InvokeSafe(loading.progress);
                
                yield return null;
            }

            callBack.InvokeSafe();
            
            this.LogWarning($"{name} : Loaded");
        }

        protected virtual void SceneLoading(string sceneName)
        {
            
        }

        protected virtual void SceneLoaded(string sceneName)
        {
            
        }

        public virtual void Init()
        {
            
        }
    }
}
